package com.conygre.training.tradesimulator.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Trade {

    @Id
    private String id;
    private String name;
    private int quantity;
    private double price;
    
    //TRADE SPECIFICS
    private Date tradeStarted = new Date(System.currentTimeMillis());
    private TradeType tradeType;
    private TradeState tradeState;

    /**
     * @param id
     * @param name
     * @param quantity
     * @param price
     * @param tradeStarted
     * @param tradeType
     * @param tradeState
     */
    public Trade(String id, String name, int quantity, double price, Date tradeStarted, TradeType tradeType,
            TradeState tradeState) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.tradeStarted = tradeStarted;
        this.tradeType = tradeType;
        this.tradeState = tradeState;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the tradeStarted
     */
    public Date gettradeStarted() {
        return tradeStarted;
    }

    /**
     * @param tradeStarted the tradeStarted to set
     */
    public void settradeStarted(Date tradeStarted) {
        this.tradeStarted = tradeStarted;
    }

    /**
     * @return the tradeType
     */
    public TradeType getTradeType() {
        return tradeType;
    }

    /**
     * @param tradeType the tradeType to set
     */
    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * @return the tradeState
     */
    public TradeState getTradeState() {
        return tradeState;
    }

    /**
     * @param tradeState the tradeState to set
     */
    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Trade [id=" + id + ", name=" + name + ", price=" + price + ", quantity=" + quantity + ", tradeState="
                + tradeState + ", tradeType=" + tradeType + ", tradeStarted=" + tradeStarted + "]";
    }   

    
}


// @Document
// public class Trade {

//     @Id
//     private String _id;
//     private Date created = new Date(System.currentTimeMillis());
//     private TradeState state = TradeState.CREATED;
//     private TradeType type = TradeType.BUY;
//     private String ticker;
//     private int quantity;
//     private double price;


//     public Date getCreated() {
//         return created;
//     }

//     public void setCreated(Date created) {
//         this.created = created;
//     }

//     public TradeState getState() {
//         return state;
//     }

//     public void setState(TradeState state) {
//         this.state = state;
//     }

//     public String getTicker() {
//         return ticker;
//     }

//     public void setTicker(String ticker) {
//         this.ticker = ticker;
//     }

//     public double getQuantity() {
//         return quantity;
//     }

//     public void setQuantity(int quantity) {
//         this.quantity = quantity;
//     }

//     public TradeType getType() {
//         return type;
//     }

//     public void setType(TradeType type) {
//         this.type = type;
//     }

//     public double getPrice() {
//         return price;
//     }

//     public void setPrice(double price) {
//         this.price = price;
//     }
// }
